<?php
/*
Plugin Name: Centaur Trails
Plugin URI: https://compion.nl
Description: Voegt essentiele maatwerkfunctionaliteiten toe
Version: 1.0
Author: Compion
Author URI: https://compion.nl
License: A "Slug" license name e.g. GPL2
*/

/* Prevent direct access to the plugin */
if ( !defined( 'ABSPATH' ) ) {
	die( "Sorry, you are not allowed to access this page directly." );
}


foreach ( glob( plugin_dir_path( __FILE__ ) . "inc/*.php" ) as $file ) {
	include_once $file;
}