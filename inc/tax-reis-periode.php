<?php 

// Register Custom Taxonomy
function reis_periode() {

	$labels = array(
		'name'                       => _x( 'Reisperiode', 'Taxonomy General Name', 'reis_periode' ),
		'singular_name'              => _x( 'Reisperiode', 'Taxonomy Singular Name', 'reis_periode' ),
		'menu_name'                  => __( 'Reisperiode', 'reis_periode' ),
		'all_items'                  => __( 'All Items', 'reis_periode' ),
		'parent_item'                => __( 'Parent Item', 'reis_periode' ),
		'parent_item_colon'          => __( 'Parent Item:', 'reis_periode' ),
		'new_item_name'              => __( 'New Item Name', 'reis_periode' ),
		'add_new_item'               => __( 'Add New Item', 'reis_periode' ),
		'edit_item'                  => __( 'Edit Item', 'reis_periode' ),
		'update_item'                => __( 'Update Item', 'reis_periode' ),
		'view_item'                  => __( 'View Item', 'reis_periode' ),
		'separate_items_with_commas' => __( 'Separate items with commas', 'reis_periode' ),
		'add_or_remove_items'        => __( 'Add or remove items', 'reis_periode' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'reis_periode' ),
		'popular_items'              => __( 'Popular Items', 'reis_periode' ),
		'search_items'               => __( 'Search Items', 'reis_periode' ),
		'not_found'                  => __( 'Not Found', 'reis_periode' ),
		'no_terms'                   => __( 'No items', 'reis_periode' ),
		'items_list'                 => __( 'Items list', 'reis_periode' ),
		'items_list_navigation'      => __( 'Items list navigation', 'reis_periode' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
		'show_in_rest'               => true,
		'yarpp_support' 		=> true,
		'rewrite' => array( 'slug' => 'periode', 'with_front' => false ),
	);
	register_taxonomy( 'reis_periode', array( 'reizen' ), $args );

}
add_action( 'init', 'reis_periode', 0 );