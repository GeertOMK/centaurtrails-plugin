<?php 

// Register Custom Taxonomy
function niveau_ruiter() {

	$labels = array(
		'name'                       => _x( 'Niveau ruiter', 'Taxonomy General Name', 'niveau_ruiter' ),
		'singular_name'              => _x( 'Niveau ruiter', 'Taxonomy Singular Name', 'niveau_ruiter' ),
		'menu_name'                  => __( 'Niveau ruiter', 'niveau_ruiter' ),
		'all_items'                  => __( 'All Items', 'niveau_ruiter' ),
		'parent_item'                => __( 'Parent Item', 'niveau_ruiter' ),
		'parent_item_colon'          => __( 'Parent Item:', 'niveau_ruiter' ),
		'new_item_name'              => __( 'New Item Name', 'niveau_ruiter' ),
		'add_new_item'               => __( 'Add New Item', 'niveau_ruiter' ),
		'edit_item'                  => __( 'Edit Item', 'niveau_ruiter' ),
		'update_item'                => __( 'Update Item', 'niveau_ruiter' ),
		'view_item'                  => __( 'View Item', 'niveau_ruiter' ),
		'separate_items_with_commas' => __( 'Separate items with commas', 'niveau_ruiter' ),
		'add_or_remove_items'        => __( 'Add or remove items', 'niveau_ruiter' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'niveau_ruiter' ),
		'popular_items'              => __( 'Popular Items', 'niveau_ruiter' ),
		'search_items'               => __( 'Search Items', 'niveau_ruiter' ),
		'not_found'                  => __( 'Not Found', 'niveau_ruiter' ),
		'no_terms'                   => __( 'No items', 'niveau_ruiter' ),
		'items_list'                 => __( 'Items list', 'niveau_ruiter' ),
		'items_list_navigation'      => __( 'Items list navigation', 'niveau_ruiter' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
		'show_in_rest'               => true,
		'yarpp_support' 		=> true,
		'rewrite' => array( 'slug' => 'niveau', 'with_front' => false ),
	);
	register_taxonomy( 'niveau_ruiter', array( 'reizen' ), $args );

}
add_action( 'init', 'niveau_ruiter', 0 );