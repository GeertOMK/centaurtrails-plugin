<?php 

// Register Custom Taxonomy
function reis_accommodatie() {

	$labels = array(
		'name'                       => _x( 'Accommodatie', 'Taxonomy General Name', 'reis_accommodatie' ),
		'singular_name'              => _x( 'Accommodatie', 'Taxonomy Singular Name', 'reis_accommodatie' ),
		'menu_name'                  => __( 'Accommodatie', 'reis_accommodatie' ),
		'all_items'                  => __( 'All Items', 'reis_accommodatie' ),
		'parent_item'                => __( 'Parent Item', 'reis_accommodatie' ),
		'parent_item_colon'          => __( 'Parent Item:', 'reis_accommodatie' ),
		'new_item_name'              => __( 'New Item Name', 'reis_accommodatie' ),
		'add_new_item'               => __( 'Add New Item', 'reis_accommodatie' ),
		'edit_item'                  => __( 'Edit Item', 'reis_accommodatie' ),
		'update_item'                => __( 'Update Item', 'reis_accommodatie' ),
		'view_item'                  => __( 'View Item', 'reis_accommodatie' ),
		'separate_items_with_commas' => __( 'Separate items with commas', 'reis_accommodatie' ),
		'add_or_remove_items'        => __( 'Add or remove items', 'reis_accommodatie' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'reis_accommodatie' ),
		'popular_items'              => __( 'Popular Items', 'reis_accommodatie' ),
		'search_items'               => __( 'Search Items', 'reis_accommodatie' ),
		'not_found'                  => __( 'Not Found', 'reis_accommodatie' ),
		'no_terms'                   => __( 'No items', 'reis_accommodatie' ),
		'items_list'                 => __( 'Items list', 'reis_accommodatie' ),
		'items_list_navigation'      => __( 'Items list navigation', 'reis_accommodatie' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
		'show_in_rest'               => true,
		'yarpp_support' 			=> true,
		'rewrite' => array( 'slug' => 'accommodatie', 'with_front' => false ),
	);
	register_taxonomy( 'reis_accommodatie', array( 'reizen' ), $args );

}
add_action( 'init', 'reis_accommodatie', 0 );