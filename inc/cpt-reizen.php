<?php 

 // Register Custom Post Type
function reizen() {

	$labels = array(
		'name'                  => _x( 'Reizen', 'Post Type General Name', 'reizen' ),
		'singular_name'         => _x( 'Reis', 'Post Type Singular Name', 'reizen' ),
		'menu_name'             => __( 'Reizen', 'reizen' ),
		'name_admin_bar'        => __( 'Reizen', 'reizen' ),
		'archives'              => __( 'Reizen archief', 'reizen' ),
		'attributes'            => __( 'Reis attributen', 'reizen' ),
		'parent_item_colon'     => __( 'Hoofdreis:', 'reizen' ),
		'all_items'             => __( 'Alle reizen', 'reizen' ),
		'add_new_item'          => __( 'Nieuwe reis', 'reizen' ),
		'add_new'               => __( 'Nieuwe toevoegen', 'reizen' ),
		'new_item'              => __( 'Nieuwe reis', 'reizen' ),
		'edit_item'             => __( 'Bewerk reis', 'reizen' ),
		'update_item'           => __( 'Update reis', 'reizen' ),
		'view_item'             => __( 'Bekijk reis', 'reizen' ),
		'view_items'            => __( 'Bekijk reizen', 'reizen' ),
		'search_items'          => __( 'Zoek een reis', 'reizen' ),
		'not_found'             => __( 'Reis niet gevonden', 'reizen' ),
		'not_found_in_trash'    => __( 'Niet gevonden in de prullenbak', 'reizen' ),
		'featured_image'        => __( 'Hoofdafbeelding', 'reizen' ),
		'set_featured_image'    => __( 'Hoofdafbeelding kiezen', 'reizen' ),
		'remove_featured_image' => __( 'Hoofdafbeelding verwijderen', 'reizen' ),
		'use_featured_image'    => __( 'Gebruiken als hoofdafbeelding', 'reizen' ),
		'insert_into_item'      => __( 'Toevoegen aan de reis', 'reizen' ),
		'uploaded_to_this_item' => __( 'Uploaden naar de reis', 'reizen' ),
		'items_list'            => __( 'Reizen lijst', 'reizen' ),
		'items_list_navigation' => __( 'Reizen lijst navigatie', 'reizen' ),
		'filter_items_list'     => __( 'Filter reizen', 'reizen' ),
	);
	$args = array(
		'label'                 => __( 'reis', 'reizen' ),
		'description'           => __( 'Post Type Description', 'reizen' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'revisions', 'thumbnail' ),
		'taxonomies'            => array( ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-palmtree',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => 'reizen',
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
		'show_in_rest'          => true,
		'yarpp_support' 		=> true,
		'rewrite' 				=> true,
	);
	register_post_type( 'reizen', $args );

}
add_action( 'init', 'reizen', 0 );