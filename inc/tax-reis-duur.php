<?php 

// Register Custom Taxonomy
function reis_duur() {

	$labels = array(
		'name'                       => _x( 'Reisduur', 'Taxonomy General Name', 'reis_duur' ),
		'singular_name'              => _x( 'Reisduur', 'Taxonomy Singular Name', 'reis_duur' ),
		'menu_name'                  => __( 'Reisduur', 'reis_duur' ),
		'all_items'                  => __( 'All Items', 'reis_duur' ),
		'parent_item'                => __( 'Parent Item', 'reis_duur' ),
		'parent_item_colon'          => __( 'Parent Item:', 'reis_duur' ),
		'new_item_name'              => __( 'New Item Name', 'reis_duur' ),
		'add_new_item'               => __( 'Add New Item', 'reis_duur' ),
		'edit_item'                  => __( 'Edit Item', 'reis_duur' ),
		'update_item'                => __( 'Update Item', 'reis_duur' ),
		'view_item'                  => __( 'View Item', 'reis_duur' ),
		'separate_items_with_commas' => __( 'Separate items with commas', 'reis_duur' ),
		'add_or_remove_items'        => __( 'Add or remove items', 'reis_duur' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'reis_duur' ),
		'popular_items'              => __( 'Popular Items', 'reis_duur' ),
		'search_items'               => __( 'Search Items', 'reis_duur' ),
		'not_found'                  => __( 'Not Found', 'reis_duur' ),
		'no_terms'                   => __( 'No items', 'reis_duur' ),
		'items_list'                 => __( 'Items list', 'reis_duur' ),
		'items_list_navigation'      => __( 'Items list navigation', 'reis_duur' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
		'show_in_rest'               => true,
		'yarpp_support' 		=> true,
		'rewrite' => array( 'slug' => 'reisduur', 'with_front' => false ),
	);
	register_taxonomy( 'reis-duur', array( 'reizen' ), $args );

}
add_action( 'init', 'reis_duur', 0 );