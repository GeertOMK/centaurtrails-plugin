<?php 

// Register Custom Taxonomy
function reis_land() {

	$labels = array(
		'name'                       => _x( 'Landen', 'Taxonomy General Name', 'reis_land' ),
		'singular_name'              => _x( 'Land', 'Taxonomy Singular Name', 'reis_land' ),
		'menu_name'                  => __( 'Landen', 'reis_land' ),
		'all_items'                  => __( 'All Items', 'reis_land' ),
		'parent_item'                => __( 'Parent Item', 'reis_land' ),
		'parent_item_colon'          => __( 'Parent Item:', 'reis_land' ),
		'new_item_name'              => __( 'New Item Name', 'reis_land' ),
		'add_new_item'               => __( 'Add New Item', 'reis_land' ),
		'edit_item'                  => __( 'Edit Item', 'reis_land' ),
		'update_item'                => __( 'Update Item', 'reis_land' ),
		'view_item'                  => __( 'View Item', 'reis_land' ),
		'separate_items_with_commas' => __( 'Separate items with commas', 'reis_land' ),
		'add_or_remove_items'        => __( 'Add or remove items', 'reis_land' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'reis_land' ),
		'popular_items'              => __( 'Popular Items', 'reis_land' ),
		'search_items'               => __( 'Search Items', 'reis_land' ),
		'not_found'                  => __( 'Not Found', 'reis_land' ),
		'no_terms'                   => __( 'No items', 'reis_land' ),
		'items_list'                 => __( 'Items list', 'reis_land' ),
		'items_list_navigation'      => __( 'Items list navigation', 'reis_land' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => false,
		'public'                     => true,
		'show_ui'                    => true,
		'rewrite'      				=> true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
		'show_in_rest'               => true,
		'rewrite' => array( 'slug' => 'land', 'with_front' => false ),
		'yarpp_support' 		=> true,
	);
	register_taxonomy( 'reis_land', array( 'reizen' ), $args );

}
add_action( 'init', 'reis_land', 0 );