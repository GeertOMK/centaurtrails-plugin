<?php 

 // Register Custom Post Type
function reviews() {

	$labels = array(
		'name'                  => _x( 'Reviews', 'Post Type General Name', 'reviews' ),
		'singular_name'         => _x( 'Review', 'Post Type Singular Name', 'reviews' ),
		'menu_name'             => __( 'Reviews', 'reviews' ),
		'name_admin_bar'        => __( 'Reviews', 'reviews' ),
		'archives'              => __( 'Review archief', 'reviews' ),
		'attributes'            => __( 'Review attributen', 'reviews' ),
		'parent_item_colon'     => __( 'Hoofdreis:', 'reviews' ),
		'all_items'             => __( 'Alle reviews', 'reviews' ),
		'add_new_item'          => __( 'Nieuwe review', 'reviews' ),
		'add_new'               => __( 'Nieuwe toevoegen', 'reviews' ),
		'new_item'              => __( 'Nieuwe review', 'reviews' ),
		'edit_item'             => __( 'Bewerk review', 'reviews' ),
		'update_item'           => __( 'Update review', 'reviews' ),
		'view_item'             => __( 'Bekijk review', 'reviews' ),
		'view_items'            => __( 'Bekijk review', 'reviews' ),
		'search_items'          => __( 'Zoek een review', 'reviews' ),
		'not_found'             => __( 'Review niet gevonden', 'reviews' ),
		'not_found_in_trash'    => __( 'Niet gevonden in de prullenbak', 'reviews' ),
		'featured_image'        => __( 'Hoofdafbeelding', 'reviews' ),
		'set_featured_image'    => __( 'Hoofdafbeelding kiezen', 'reviews' ),
		'remove_featured_image' => __( 'Hoofdafbeelding verwijderen', 'reviews' ),
		'use_featured_image'    => __( 'Gebruiken als hoofdafbeelding', 'reviews' ),
		'insert_into_item'      => __( 'Toevoegen aan de review', 'reviews' ),
		'uploaded_to_this_item' => __( 'Uploaden naar de review', 'reviews' ),
		'items_list'            => __( 'review lijst', 'reviews' ),
		'items_list_navigation' => __( 'review lijst navigatie', 'reviews' ),
		'filter_items_list'     => __( 'Filter review', 'reviews' ),
	);
	$args = array(
		'label'                 => __( 'review', 'reviews' ),
		'description'           => __( 'Post Type Description', 'reviews' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'excerpt', 'revisions', 'thumbnail' ),
		'taxonomies'            => array( 'category', 'post_tag' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-heart',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => 'reviews',
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
		'show_in_rest'          => true,
		'yarpp_support' 		=> true,
	);
	register_post_type( 'reviews', $args );

}
add_action( 'init', 'reviews', 0 );