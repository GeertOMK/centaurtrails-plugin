<?php 

// Register Custom Taxonomy
function reis_soort() {

	$labels = array(
		'name'                       => _x( 'Soort reis', 'Taxonomy General Name', 'reis_soort' ),
		'singular_name'              => _x( 'Soort reis', 'Taxonomy Singular Name', 'reis_soort' ),
		'menu_name'                  => __( 'Soort reis', 'reis_soort' ),
		'all_items'                  => __( 'All Items', 'reis_soort' ),
		'parent_item'                => __( 'Parent Item', 'reis_soort' ),
		'parent_item_colon'          => __( 'Parent Item:', 'reis_soort' ),
		'new_item_name'              => __( 'New Item Name', 'reis_soort' ),
		'add_new_item'               => __( 'Add New Item', 'reis_soort' ),
		'edit_item'                  => __( 'Edit Item', 'reis_soort' ),
		'update_item'                => __( 'Update Item', 'reis_soort' ),
		'view_item'                  => __( 'View Item', 'reis_soort' ),
		'separate_items_with_commas' => __( 'Separate items with commas', 'reis_soort' ),
		'add_or_remove_items'        => __( 'Add or remove items', 'reis_soort' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'reis_soort' ),
		'popular_items'              => __( 'Popular Items', 'reis_soort' ),
		'search_items'               => __( 'Search Items', 'reis_soort' ),
		'not_found'                  => __( 'Not Found', 'reis_soort' ),
		'no_terms'                   => __( 'No items', 'reis_soort' ),
		'items_list'                 => __( 'Items list', 'reis_soort' ),
		'items_list_navigation'      => __( 'Items list navigation', 'reis_soort' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
		'show_in_rest'               => true,
		'yarpp_support' 		=> true,
		'rewrite' => array( 'slug' => 'soort-reis', 'with_front' => false ),
	);
	register_taxonomy( 'reis_soort', array( 'reizen' ), $args );

}
add_action( 'init', 'reis_soort', 0 );