<?php 

// Register Custom Taxonomy
function reis_overig() {

	$labels = array(
		'name'                       => _x( 'Overig', 'Taxonomy General Name', 'reis_overig' ),
		'singular_name'              => _x( 'Overig', 'Taxonomy Singular Name', 'reis_overig' ),
		'menu_name'                  => __( 'Overig', 'reis_overig' ),
		'all_items'                  => __( 'All Items', 'reis_overig' ),
		'parent_item'                => __( 'Parent Item', 'reis_overig' ),
		'parent_item_colon'          => __( 'Parent Item:', 'reis_overig' ),
		'new_item_name'              => __( 'New Item Name', 'reis_overig' ),
		'add_new_item'               => __( 'Add New Item', 'reis_overig' ),
		'edit_item'                  => __( 'Edit Item', 'reis_overig' ),
		'update_item'                => __( 'Update Item', 'reis_overig' ),
		'view_item'                  => __( 'View Item', 'reis_overig' ),
		'separate_items_with_commas' => __( 'Separate items with commas', 'reis_overig' ),
		'add_or_remove_items'        => __( 'Add or remove items', 'reis_overig' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'reis_overig' ),
		'popular_items'              => __( 'Popular Items', 'reis_overig' ),
		'search_items'               => __( 'Search Items', 'reis_overig' ),
		'not_found'                  => __( 'Not Found', 'reis_overig' ),
		'no_terms'                   => __( 'No items', 'reis_overig' ),
		'items_list'                 => __( 'Items list', 'reis_overig' ),
		'items_list_navigation'      => __( 'Items list navigation', 'reis_overig' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
		'show_in_rest'               => true,
		'yarpp_support' 		=> true,
		'rewrite' => array( 'slug' => 'overig', 'with_front' => false ),
	);
	register_taxonomy( 'reis_overig', array( 'reizen' ), $args );

}
add_action( 'init', 'reis_overig', 0 );