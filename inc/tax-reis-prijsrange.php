<?php 

// Register Custom Taxonomy
function prijsrange() {

	$labels = array(
		'name'                       => _x( 'Prijsrange', 'Taxonomy General Name', 'prijsrange' ),
		'singular_name'              => _x( 'Prijsrange', 'Taxonomy Singular Name', 'prijsrange' ),
		'menu_name'                  => __( 'Prijsrange', 'prijsrange' ),
		'all_items'                  => __( 'All Items', 'prijsrange' ),
		'parent_item'                => __( 'Parent Item', 'prijsrange' ),
		'parent_item_colon'          => __( 'Parent Item:', 'prijsrange' ),
		'new_item_name'              => __( 'New Item Name', 'prijsrange' ),
		'add_new_item'               => __( 'Add New Item', 'prijsrange' ),
		'edit_item'                  => __( 'Edit Item', 'prijsrange' ),
		'update_item'                => __( 'Update Item', 'prijsrange' ),
		'view_item'                  => __( 'View Item', 'prijsrange' ),
		'separate_items_with_commas' => __( 'Separate items with commas', 'prijsrange' ),
		'add_or_remove_items'        => __( 'Add or remove items', 'prijsrange' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'prijsrange' ),
		'popular_items'              => __( 'Popular Items', 'prijsrange' ),
		'search_items'               => __( 'Search Items', 'prijsrange' ),
		'not_found'                  => __( 'Not Found', 'prijsrange' ),
		'no_terms'                   => __( 'No items', 'prijsrange' ),
		'items_list'                 => __( 'Items list', 'prijsrange' ),
		'items_list_navigation'      => __( 'Items list navigation', 'prijsrange' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
		'show_in_rest'               => true,
		'yarpp_support' 		=> true,
		'rewrite' => array( 'slug' => 'prijsrange', 'with_front' => false ),
	);
	register_taxonomy( 'prijsrange', array( 'reizen' ), $args );

}
add_action( 'init', 'prijsrange', 0 );